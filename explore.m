close all
%% Setup environment
%setup_environment
%% Setup map
setup_map
%% Setup display
setup_display
%% Setting up observation properties
setup_observation_properties
%% Setup nodes
setup_nodes
%% Setting up reward function
setup_reward_function
%% Setup rro
setup_rro

fig_handle_scatter = figure;

history = curr_pose;
waypoints = [];
lookahead_distance = 0;
prev_best_route = [];
counter = 0;
%% big loop
while 1%norm(curr_pose - nodes(end_node_id,:)) > velocity
    %% Updating reward function
    setup_reward_function
    
    %% Plan    
    D = distmat(nodes);
    lookahead = get_lookahead(curr_pose, waypoints,lookahead_distance);

    nodes(1,:) = lookahead;
    start_node_id = 1;
    dd = path_length(history);
    left_budget = budget - dd(end);
    display('New loop')
    [route_list, best_route_id] = rro( nodes, D, start_node_id, end_node_id, left_budget, run_time_budget, reward_fn, sampling_func_handle, route_selection_func_handle, threshold_distance, display_flag);
    if(isempty(best_route_id))
        best_route_id = 1;
    end
    best_route = route_list(best_route_id);
    
    if ~isempty(prev_best_route)
        prev_best_route  = evaluate_route(prev_best_route.route, nodes,reward_fn);
        if(prev_best_route.reward > (0.9*route_list(best_route_id).reward))
            best_route = prev_best_route;
        end
    end
    
    small_loop_time_res =max(1,run_time_budget/3);
    distance_to_move = 5;
    waypoints = nodes(best_route.route(:),:);
    %% small loop
    for i = small_loop_time_res:small_loop_time_res:run_time_budget
        %% Observe
        [ loc_ids, observed_positions ] = get_visible_ids(curr_pose,camera_handle,semantic_map,map_struct);
        [ meas_id, loc_ids ] = get_most_probable_meas( loc_ids, ground_truth_semantic_map );

        %% Update
        
        observation_distances = bsxfun(@minus,[observed_positions, zeros(length(loc_ids),1)],curr_pose);
        observation_distances = sqrt(sum(observation_distances.*observation_distances,2));
        
        [ semantic_map, ~ ] = update_semantic_map_exploration(loc_ids, meas_id, observation_distances,class_reward_distances,confusion_matrices, confusion_distances,semantic_map);        

        %% Display
        set(0,'CurrentFigure',disp_obj.fig_handles(2));
        clf
        pre_computed_vertices = display_semantic_map( semantic_map, colors, map_struct,pre_computed_vertices);
        hold on
        display_robot( curr_pose, robot_color, robot_style, robot_size );
        plot(history(:,1),history(:,2),'LineWidth',4, 'Color', [1.0 0.2 0.2])
        plot(observed_positions(:,1),observed_positions(:,2),'.y', 'MarkerSize',10)
        set(gcf, 'Position', [0, 100, 1080, 700]);
        set(gcf,'PaperPositionMode','auto')
        print(strcat('results/',num2str(counter,'%04d'),'_semantic'),'-dpng','-r0')
        
        rewards = zeros(size(nodes,1),1);
        for i=1:size(nodes,1)
            rewards(i) = reward_fn(nodes(i,:),[]);
        end
        set(0,'CurrentFigure',fig_handle_scatter);
        clf
        scatter(nodes(:,1),nodes(:,2),1000*ones(size(nodes,1),1),(rewards-min(rewards))/max(rewards),'filled')
        hold on
        display_graph( nodes,best_route, 1, [] );
        set(gcf, 'Position', [1000, 100, 1080, 700]);
        set(gcf,'PaperPositionMode','auto')
        print(strcat('results/',num2str(counter,'%04d'),'_graph'),'-dpng','-r0')
        pause(0.1)

        %% Move        
        [curr_pose, waypoints] = move_robot( curr_pose, waypoints, distance_to_move );
        history = [history;curr_pose];
        prev_best_route = best_route;
        relevant_index = length(best_route.route) - size(waypoints,1);
        relevant_index = max([relevant_index,1]);
        prev_best_route.route = [1,route_list(best_route_id).route(relevant_index:end)];
        counter = counter+1;
    end
    
    %% Updating Graph
    new_nodes = generate_nodes( reward_coverage_maps, semantic_map, class_reward_distances, class_threshold_prob, curr_pose, map_struct);
    [ reward_coverage_maps, visibility, nodes] = add_new_nodes( new_nodes, nodes,visibility, reward_coverage_maps,camera_handle, class_reward_distances,semantic_map,map_struct);

end