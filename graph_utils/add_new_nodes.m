function [ reward_coverage_maps, visibility, nodes] = add_new_nodes( new_nodes, old_nodes,old_visibility, old_reward_coverage_maps,camera_handle, class_reward_distances,semantic_maps,map_struct)
%ADD_NEW_NODES Summary of this function goes here
%   Detailed explanation goes here
[ reward_coverage_maps, visibility ] = pre_process_node( new_nodes,camera_handle,class_reward_distances,semantic_maps,map_struct );
nodes = [old_nodes;new_nodes];
visibility = [old_visibility,visibility];
for i=1:length(class_reward_distances)
    if(class_reward_distances(i)>0)
        reward_coverage_maps(i).map(reward_coverage_maps(i).map>0) = size(old_nodes,1) + reward_coverage_maps(i).map(reward_coverage_maps(i).map>0);
        reward_coverage_map(i).map = max(old_reward_coverage_maps(i).map, reward_coverage_maps(i).map);
    end
end
end