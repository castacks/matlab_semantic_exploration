function [ nodes ] = generate_nodes( reward_coverage_maps, semantic_map, class_reward_distances, class_probability_thresholds, curr_pose, map_struct)
%GENERATE_NODES Summary of this function goes here
%   Detailed explanation goes here
nodes = [];
for i=1:length(class_probability_thresholds)
    if(class_probability_thresholds(i)>0)
         ID = find(semantic_map.p(:,i)>= class_probability_thresholds(i));
         C_ID = find(reward_coverage_maps(i).map>0);
         ID = setdiff(ID,C_ID);
         ID = ID(semantic_map.class_coverage(i).map(ID) == 0);
         if(~isempty(ID))
             new_nodes = ID2Node(ID, map_struct, semantic_map, class_reward_distances(i), curr_pose);
             nodes = [nodes;new_nodes];
         end
    end
end
end

function [nodes] = ID2Node(ID, map_struct, semantic_map, distance, curr_pose)
    [I,J] = ind2sub(size(semantic_map.id_map),ID);
    nodes = map_struct.scale*[I, J, zeros(size(I))];
    v = bsxfun(@minus,curr_pose,nodes);
    v = bsxfun(@rdivide,v,sqrt(sum(v.*v,2)));
    nodes = nodes + 0.9*distance*v;
end