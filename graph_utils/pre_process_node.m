function [ reward_coverage_maps, visibility ] = pre_process_node( nodes,camera_handle,class_reward_distances,semantic_maps,map_struct )
%PRE_PROCESS_NODE Summary of this function goes here
%   Detailed explanation goes here

%reward_coverage_maps(n).map where n is the number of classes and each cell says
%which node covers it

%visibility has visibility in the sense of visible ids and distances but
%now will also contain class_visisibility_maps
%visibility.class_visibility(n).visible_ids
visibility = [];
reward_coverage_maps = [];

for  i=1:size(nodes,1)
    [ loc_ids, observed_positions ] = get_visible_ids(nodes(i,:),camera_handle,semantic_maps,map_struct);
    observed_positions = [observed_positions, zeros(size(observed_positions,1),1)];
    
    distances = bsxfun(@minus,observed_positions,nodes(i,:));
    distances =sqrt(sum(distances.*distances,2));
    visibility(i).visible_ids = loc_ids;
    visibility(i).distances = distances;
end

for j=1:length(class_reward_distances)
    reward_coverage_maps(j).map = zeros(size(semantic_maps.id_map));    
end


for i=1:size(nodes,1)
    for j=1:length(class_reward_distances)
        if(class_reward_distances(j)>0)
            distance_camera_handle = @(pose,map_struct_var)(distance_camera( pose, class_reward_distances(j), map_struct_var ));
            [ loc_ids, ~ ] = get_visible_ids(nodes(i,:),distance_camera_handle,semantic_maps,map_struct);
            reward_coverage_maps(j).map(loc_ids(:)) = i;
            visibility(i).class_visibility(j).visible_ids = loc_ids;
        end
    end
end