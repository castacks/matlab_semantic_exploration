function [ route_list ] = evaluate_route(routes, nodes,reward_func_handle)
%EVALUATE_ROUTE Summary of this function goes here
%   Detailed explanation goes here

for i=1:size(routes,1)
   cost =  path_length(nodes(routes(i,:),:));
   [reward, func_state] = reward_func_handle(nodes(routes(i,:),:),[]);
   route_list(i).cost = cost(end);
   route_list(i).reward = reward;
   route_list(i).reward_func_input = func_state;
   route_list(i).route = routes(i,:);
end

