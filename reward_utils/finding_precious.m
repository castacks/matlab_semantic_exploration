function [ reward, func_state ] = finding_precious( nodes, semantic_map, class_rewards, ob_distance_threshold, ob_probability_threshold, map_struct, func_state)
%FINDING_PRECIOUS Summary of this function goes here
%   Detailed explanation goes here
% semantic_map.covered;
if (isempty(func_state) || isempty(func_state.semantic_map))
    sm = semantic_map;
else
    sm = func_state.semantic_map;
end

reward = 0;
for i=1:size(nodes,1)
    for j=1:length(ob_distance_threshold)
       if(ob_distance_threshold(j)>0)
           distance_camera_handle = @(pose,map_struct_var)(distance_camera( pose, ob_distance_threshold(j), map_struct_var ));
            [ all_loc_ids, ~ ] = get_visible_ids(nodes(i,:),distance_camera_handle,sm,map_struct);
            loc_ids = sm.id_map(all_loc_ids);
            
            loc_ids = loc_ids(sm.p(loc_ids,j)>ob_probability_threshold(j));
            loc_ids = loc_ids(sm.class_coverage(j).map(loc_ids)<1);
            
            temp_reward = class_rewards(j)*sum(length(loc_ids).*sm.p(loc_ids,j));
            reward = reward + temp_reward;
            sm.class_coverage(j).map(all_loc_ids(:)) = 1;
       end
    end
end
func_state.semantic_map = sm;
end