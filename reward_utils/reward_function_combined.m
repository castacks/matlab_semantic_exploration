function [ reward, func_state ] = reward_function_combined( q_nodes, nodes, visibility, semantic_map, observation_model, func_state, mode, fn_handle, search_reward_handle)
%Let there be n nodes, D be the dimension of nodes. 
%nodes is nXD

%semantic_map has id_map (MXM) and p (M^2)XC priors

%visibility is an array of structure, visible_ids and corresponding
%distances

%func_state.evaluated_nodes
%func_state.semantic_map

%There are 2 modes 'expected reward and  the most probable reward'
% for now we will only support most probable reward

%Observation model is a structure
%observation_model.confusion_matrice
%observation_model.observation_distances -- this is sorted in ascending
%order

if(strcmp(mode,'expected'))
   error('expected mode is not supported yet')
elseif(~strcmp(mode,'probable'))
    error('Wrong mode input')
end

if isempty(func_state) || isempty(func_state.semantic_map)
    s_m = semantic_map;
    func_state.evaluated_nodes = zeros(size(nodes,1),1);
    func_state.search_reward = [];
else
    s_m = func_state.semantic_map;
end

confusion_matrices = observation_model.confusion_matrice;
confusion_distances = observation_model.observation_distances;
semantic_map = s_m;
changed_loc_id = [];

for i=1:size(q_nodes,1)
    comp=any(bsxfun(@minus,nodes,q_nodes(i,:)),2);
    id = find(comp==0);
    if func_state.evaluated_nodes(id)==0
        loc_id = visibility(id).visible_ids;
        observation_distances = visibility(id).distances;
        [ meas_id, ~ ] = get_most_probable_meas( loc_id, s_m);
        [ semantic_map, ~ ] = update_semantic_map_mat(loc_id, meas_id, observation_distances,confusion_matrices, confusion_distances,semantic_map);
        func_state.evaluated_nodes(id) =  func_state.evaluated_nodes(id)+ 1;
        changed_loc_id = [changed_loc_id; loc_id];
    end
end
changed_loc_id = unique(changed_loc_id);
prior = s_m.p(changed_loc_id,:);
posterior = semantic_map.p(changed_loc_id,:);
reward = sum(fn_handle(prior,posterior));
func_state.semantic_map  = semantic_map;

search_reward = 0;
for i=1:size(q_nodes,1)
    [ temp_search_reward, func_state.search_reward ] = search_reward_handle(q_nodes(i,:),func_state.search_reward);
    search_reward = search_reward + temp_search_reward;
end

reward = reward + search_reward;

end