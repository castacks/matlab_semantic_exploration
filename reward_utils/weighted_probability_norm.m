function [ weighted_distance ] = weighted_probability_norm( posterior, prior, weight )
%WEIGHTED_PROBABILITY_NORM Summary of this function goes here
%   Detailed explanation goes here
d = (posterior - prior);
d = bsxfun(@times,abs(d),weight);
weighted_distance = sum(d,2);
end