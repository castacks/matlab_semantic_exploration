disp_obj.fig_handles(1) = figure;
pre_computed_vertices = display_semantic_map( semantic_map, colors, map_struct);
ground_truth_semantic_map = semantic_map;
prior = [0.4,0.01,0.59];
semantic_map.p = bsxfun(@times,ones(size(semantic_map.p)),prior);
%prior here
disp_obj.fig_handles(2) = figure;
pre_computed_vertices = display_semantic_map( semantic_map, colors, map_struct,pre_computed_vertices);

robot_size = 70;
robot_style = '.';
robot_color = 'w';

path_color = 'y';
path_size  = 2;
node_size = 5;