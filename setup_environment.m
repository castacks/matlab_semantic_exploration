clear all
close all
addpath('utils')
addpath('reward_utils')
addpath('graph_utils')

addpath('../map_utils')
addpath('../exploration_objectives/probability_utils')
addpath('../matlab_semantic_maps')
addpath('../matlab_semantic_maps/bayes_utils')
addpath('../matlab_camera_utils')

cd ../greedy_orienteering/rro
addpath(pwd)
add_to_path

cd ../../matlab_semantic_exploration