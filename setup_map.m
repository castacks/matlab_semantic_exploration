image_path = '../map_utils/stash/semantic_map.png';
levels = [0,0,255;255,0,0;0,255,0];
colors = [0,0,1;1,0,0;0,1,0];
sub_sample = 5;
[ map ] = get_multiclass_map_from_image( image_path, levels, sub_sample);
map_struct.scale = 5; map_struct.min = [1,1];
[ semantic_map ] = generate_semantic_map( map );


semantic_map.class_coverage(1).map = zeros(size(semantic_map.id_map));
semantic_map.class_coverage(2).map = zeros(size(semantic_map.id_map));
semantic_map.class_coverage(3).map = zeros(size(semantic_map.id_map));