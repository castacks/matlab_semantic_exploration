node_height = 10;
[X,Y] = meshgrid(0.5:10:100,0.5:10:100);

nodes = [X(:), Y(:), ones(size(X(:),1),1)*node_height];

curr_pose = [1,1,node_height];
velocity = 10;

nodes = [curr_pose;nodes];

%higher nodes
higher_height = 50;
[X,Y] = meshgrid(1:20:100,1:20:100);
higher_nodes = [X(:), Y(:),ones(size(X(:),1),1)*higher_height ];

nodes = [nodes;higher_nodes];

[ reward_coverage_maps, visibility ] = pre_process_node( nodes,camera_handle,class_reward_distances,semantic_map,map_struct );

nodes(1,:) = curr_pose;
start_node_id = 1;
end_node_id = 2;