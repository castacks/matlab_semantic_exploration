%confusion_matrices = [0.6,0.2,0.2, 0.2,0.6,0.2, 0.2,0.2,0.6];
confusion_matrices = [0.99, 0.01, 0.0, 0.01, 0.99, 0.0, 0.01, 0.0, 0.99;
                      0.95, 0.03, 0.02, 0.2, 0.8, 0.1, 0.0, 0.0, 1.0;
                      0.85, 0.1, 0.05, 0.4, 0.5, 0.1, 0.05, 0.05, 0.9];
%confusion_distances = [20;100;500];
confusion_distances = [11;40;500];
camera_fov = deg2rad(120);
camera_handle = @(pose,map_struct_var)(get_circular_camerafootprint( pose, camera_fov, map_struct_var));

class_reward_distances = [0,6,0];

class_threshold_prob = [0,0.8,0];

class_rewards = [0,100,0];