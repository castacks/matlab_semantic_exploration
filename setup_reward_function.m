weight = [0,2,0];
reward_fn_handle = @(posterior,prior) weighted_probability_norm( posterior, prior, weight );
mode = 'probable';
observation_model.confusion_matrice = confusion_matrices;
observation_model.observation_distances = confusion_distances;


search_reward_handle = @(q_nodes, func_state) finding_precious( q_nodes, semantic_map, class_rewards, class_reward_distances, class_threshold_prob, map_struct, func_state);

reward_fn = @(q_nodes, func_state) reward_function_combined( q_nodes, nodes, visibility, semantic_map, observation_model, func_state, mode, reward_fn_handle, search_reward_handle);
