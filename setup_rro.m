%% setting up sampling function
sampling_func_handle = @(nodes, sampling_func_input)(weighted_node_sampling(nodes,sampling_func_input));

%% setting up route selection function handle
budget = 400;
%route_selection_func_handle = @distance_based_route_selector;
route_selection_func_handle = @best_rate_path_selection;

%% RRO
run_time_budget = 5;
display_flag = 0;
threshold_distance = 2;
