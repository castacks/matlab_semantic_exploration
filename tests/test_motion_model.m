close all
clear all
waypoints = [1,1,10;1,5,10;5,5,10;5,1,10;2.5,1,10;2.5,2.5,10;2.0,2.5,10;2.0,1,10];
curr_pose = [0,0,10];
plot(waypoints(:,1), waypoints(:,2),'b','LineWidth',4)
hold on
path_xyz = waypoints;
for i=1:10
    length_to_move = 2;
    [ curr_pose, path_xyz ] = move_robot( curr_pose, path_xyz, length_to_move );
    clf
    hold on
    plot(waypoints(:,1), waypoints(:,2),'b','LineWidth',4)
    plot(path_xyz(:,1), path_xyz(:,2),'r','LineWidth',4)
    plot(curr_pose(1), curr_pose(2),'k.','MarkerSize',50)
    axis equal
    pause
end