wp1 = [0.5,0.5,10; 0.5,10.5,10; 10.5,0.5,10]; %----- cluster

wp2 = [0.5,0.5,10; 30.5,30.5,10; 30.5,80.5,10]; %----- diagonal

wp3 = [0.5,0.5,10; 30.5,0.5,10; 80.5,0.5,10]; %-------xaxis

wp4 = [90.5,0.5,10; 90.5,20.5,10; 90.5,10.5,10]; %-------xaxis

routes = [];
wp = wp1;
for i=1:size(wp,1)
    comp=any(bsxfun(@minus,nodes,wp(i,:)),2);
    id = find(comp==0);
    routes(i) = id;
end
[ route_list ] = evaluate_route(routes, nodes,reward_fn);
sprintf('Reward:%f Cost:%f Size of route:%f', route_list.reward, route_list.cost, length(route_list.route))

routes = [];
wp = wp2;
for i=1:size(wp,1)
    comp=any(bsxfun(@minus,nodes,wp(i,:)),2);
    id = find(comp==0);
    routes(i) = id;
end
[ route_list ] = evaluate_route(routes, nodes,reward_fn);
sprintf('Reward:%f Cost:%f Size of route:%f', route_list.reward, route_list.cost, length(route_list.route))

routes = [];
wp = wp3;
for i=1:size(wp,1)
    comp=any(bsxfun(@minus,nodes,wp(i,:)),2);
    id = find(comp==0);
    routes(i) = id;
end
[ route_list ] = evaluate_route(routes, nodes,reward_fn);
sprintf('Reward:%f Cost:%f Size of route:%f', route_list.reward, route_list.cost, length(route_list.route))


display('New Testing')
routes = [];
wp = wp4(3,:);
for i=1:size(wp,1)
    comp=any(bsxfun(@minus,nodes,wp(i,:)),2);
    id = find(comp==0);
    routes(i) = id;
end
[ route_list ] = evaluate_route(routes, nodes,reward_fn);
sprintf('Reward:%f Cost:%f Size of route:%f', route_list.reward, route_list.cost, length(route_list.route))

routes = [];
wp = wp4(2:3,:);
for i=1:size(wp,1)
    comp=any(bsxfun(@minus,nodes,wp(i,:)),2);
    id = find(comp==0);
    routes(i) = id;
end
[ route_list ] = evaluate_route(routes, nodes,reward_fn);
sprintf('Reward:%f Cost:%f Size of route:%f', route_list.reward, route_list.cost, length(route_list.route))

routes = [];
wp = wp4(1:3,:);
for i=1:size(wp,1)
    comp=any(bsxfun(@minus,nodes,wp(i,:)),2);
    id = find(comp==0);
    routes(i) = id;
end
[ route_list ] = evaluate_route(routes, nodes,reward_fn);
sprintf('Reward:%f Cost:%f Size of route:%f', route_list.reward, route_list.cost, length(route_list.route))