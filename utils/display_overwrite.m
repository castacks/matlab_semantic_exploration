function [ output_args ] = display_overwrite( display_obj, fig_handle_id, fig_on_id,disp_func )
%DISPLAY_OVERWRITE Summary of this function goes here
%   Detailed explanation goes here
if(~isempty(display_obj.fig_handles(fig_handle_id)))
    delete(display_obj.fig_handles(fig_handle_id))
end

