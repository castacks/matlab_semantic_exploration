function fig_handle = display_path(path, color, node_size, path_size)
%DISPLAY_PATH Summary of this function goes here
%   Detailed explanation goes here
fig_handle = plot(path(:,1),path(:,2),color,'LineWidth',path_size,'MarkerStyle','o','MarkerSize',node_size);
end