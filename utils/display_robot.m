function [ fig_handle ] = display_robot( curr_pos, color, style, size )
%DISPLAY_ROBOT Summary of this function goes here
%   Detailed explanation goes here
fig_handle = plot(curr_pos(1),curr_pos(2),strcat(style,color),'MarkerSize',size);
end