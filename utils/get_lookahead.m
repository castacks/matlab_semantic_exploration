function [ lookahead ] = get_lookahead(curr_pose, path_xyz,lookahead)
%GET_LOOKAHEAD Summary of this function goes here
%   Detailed explanation goes here
    if(isempty(path))
        lookahead = curr_pose;
    else
        [ lookahead, ~ ] = move_robot( curr_pose, path_xyz, lookahead );
    end
end

