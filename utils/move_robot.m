function [ curr_pose, path_xyz ] = move_robot( curr_pose, path_xyz, length_to_move )
%MOVE_ROBOT Summary of this function goes here
%   Detailed explanation goes here
path_xyz = [curr_pose;path_xyz];
dd = path_length(path_xyz);
I = find(dd>=length_to_move,1,'first');

if(isempty(I)|| I==size(path_xyz,1))
    p = path_xyz(end,:);
else
    p = path_xyz(I+1,:);    
    path_xyz(1:I,:) = [];
end



d = curr_pose - p;
d = sqrt(sum(d.*d,2));

if(d <= length_to_move)
    curr_pose = p;
else
    lambda = length_to_move/d;
    dir_vec = (p-curr_pose);
    curr_pose = curr_pose + dir_vec*lambda;
end
end