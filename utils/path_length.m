function [ d ] = path_length( path )
%PATH_LENGTH Summary of this function goes here
%   Detailed explanation goes here
if(size(path,1)<2)
    d = 0;
    return;
end
p1 = path(2:end,:);
d = p1 - path(1:end-1,:);
d = cumsum(sqrt(sum(d.*d,2)));
end

