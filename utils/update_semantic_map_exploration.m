function [ semantic_map, marginal ] = update_semantic_map_exploration( ids, o_ids, observation_distances, class_reward_distances,confusion_matrices, confusion_distances,semantic_map)
%UPDATE_SEMANTIC_MAP_EXPLORATION Summary of this function goes here
%   Detailed explanation goes here
[ semantic_map, marginal ] = update_semantic_map_mat(ids, o_ids, observation_distances,confusion_matrices, confusion_distances,semantic_map);

for i=1:length(class_reward_distances)
    loc_ids = observation_distances(observation_distances<=class_reward_distances(i));
    semantic_map.class_coverage(i).map(loc_ids) = 1;
end
end